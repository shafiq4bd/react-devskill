import React, { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import ProductData from './prodct.json';
import ProductDetails from './ProductDetails';


const Product = () => {
    const [loader,setLoader] = useState(false);
    const [product,setProduct] = useState([]);
    const [productDetails,setProductDetails] = useState(null);
    useEffect(()=>{
        setProduct(ProductData);
    },[]);
    const productDetailsPage = (data) => {
        setLoader(true);
        setTimeout(() =>{
            setLoader(false);
            setProductDetails(data);
        },2000)
        
    }
    
  return (
      <div className="product">
            {
                productDetails == null ? (
                    <div>
                        <h2>Product</h2>
                        <Table striped bordered hover size="sm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    product.map((data,i) =>(
                                    <tr key={i}>
                                        <td>{i+1}</td>
                                        <td>{data.name}</td>
                                        <td>{data.price}</td>
                                        <td>
                                            {/* <button >View</button> */}
                                            <Button variant="primary" onClick={() => productDetailsPage(data)}>View</Button>
                                        </td>
                                    </tr>
                                    ))
                                }
                            </tbody>
                        </Table>
                    </div>
                ) :""
            }
            
            {
                productDetails && <ProductDetails productDetails={productDetails} productDetailsPage={productDetailsPage}  />
            }
            {
                loader === true ? (
                    <div className="spinner-box">
                        <div className="circle-border">
                            <div className="circle-core"></div>
                        </div>  
                    </div>
                ):""
            }
            
      </div>
  )
};

export default Product;
