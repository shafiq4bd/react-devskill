import React from 'react';
import { Card } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';

const ProductDetails = ({productDetails, productDetailsPage}) => {
  debugger;
  const {name, description, price} = productDetails;
  return (
    
    <div className="product_details">    
      <Card style={{ width: '18rem' }}>
        <Card.Body>
          <Card.Title>Product Details</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">{name} -  {price}</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Button variant="danger" onClick={() => productDetailsPage(null)}>Close</Button>
        </Card.Body>
    </Card>
    </div>
  )
};

export default ProductDetails;
