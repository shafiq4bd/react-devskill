import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import './App.css';
import Product from './Product';
import Task from './Task';

function App() {
  const [page, setPage] = useState("task");
  const pageRoute = (value) => {
    setPage(value);
  };
  return (
    <div className="App" dflex justifyContentCenter>

      {page === "product" && <Product /> }
      {page === "task" && <Task /> }      
      
    </div>
  );
}

export default App;
